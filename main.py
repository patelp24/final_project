import csv
from os import terminal_size
from tkinter.tix import WINDOW
from matplotlib import docstring
import pandas as pd
import networkx as nx
import matplotlib.pyplot as plt
import collections
import numpy as np

def makeG( used, color ):
    degree_sequence = sorted([d for n, d in G.degree(used)], reverse=True)
    degreCount = collections.Counter(degree_sequence)
    deg, cnt = zip(*degreCount.items())
    cs = np.cumsum(cnt) / len(used)
    plt.loglog(deg, cs, color=color)
    plt.ylabel("Number of Nodes")
    plt.ylim([0, 1])
    plt.xlabel("Degrees")
    plt.xscale("log")
    plt.legend(['human', 'virus'])

# def findPresent(type):
#     present = []
#
#     for item in human_proteins:
#         if item in type:
#             present.append(item)
#     return present

G = nx.Graph()
with open("pathlinker-human-network.txt") as f:
    contents = f.readlines()
tail = []
head = []
for line in contents:
    words = line.split()
    t = words[0].strip()
    h = words[1].strip()
    tail.append(t)
    head.append(h)
    G.add_edge(t, h)
tail2 = tail[1:]
head2 = head[1:]
data = {'Tail': tail2,
        'Head': head2}
human_data = pd.DataFrame(data)
human_proteins = list(set(head + tail))
#
# fungi_data = pd.read_csv('fungi.csv', encoding='latin1')
# human_fungiproteins = list(set(fungi_data['Uniprot ID.1']))
# fgraph = nx.Graph()
# fproteins = list(zip(fungi_data['Uniprot ID'], fungi_data['Uniprot ID.1']))
# fgraph.add_edges_from(fproteins)
#
protozoa_data = pd.read_csv('protozoa.csv', encoding='latin1')
human_protozoaproteins = list(set(protozoa_data['Uniprot ID.1']))
pgraph = nx.Graph()
pproteins = list(zip(protozoa_data['Uniprot ID'], protozoa_data['Uniprot ID.1']))
pgraph.add_edges_from(pproteins)
#
# bacteria_data = pd.read_csv('bateria.csv', encoding='latin1')
# human_bacteriaproteins = list(set(bacteria_data['Uniprot ID.1']))
# bgraph = nx.Graph()
# bproteins = list(zip(bacteria_data['Uniprot ID'], bacteria_data['Uniprot ID.1']))
# bgraph.add_edges_from(bproteins)
#
# virus_data = pd.read_csv('virus_data.csv', encoding='latin1')
# human_viralproteins = list(set(virus_data['Uniprot ID.1']))
# vgraph = nx.Graph()
# vproteins = list(zip(virus_data['Uniprot ID'], virus_data['Uniprot ID.1']))
# vgraph.add_edges_from(vproteins)
#
#
# present = []
# not_present = []
#
# v = findPresent(human_viralproteins)
# f = findPresent(human_fungiproteins)
# p = findPresent(human_protozoaproteins)
# b = findPresent(human_bacteriaproteins)
#
# makeG(G.nodes, 'red')
# makeG(v, "green")
# makeG(b, "blue")
# makeG(p, "purple")
# makeG(f, "pink")
# plt.legend(['human', 'virus', 'Bacteria', 'Protozoa', 'Fungi'])
# plt.show()
#
#


#Centrality information
'''
humancore = nx.k_core(G, 5)
centralities = nx.betweenness_centrality(humancore)

def centrality_graph(graph: nx.Graph, centralities: dict):
    cents = []
    for node in graph.nodes():
        if node in centralities.keys():
            cents.append(centralities[node])
    num_cents = len(cents)
    percents = list(range(1, num_cents+1))[::-1]
    percents = [x/num_cents for x in percents]
    dic = {'cents': sorted(cents), 'percents': percents}
    df = pd.DataFrame(dic)
    return df

df = centrality_graph(humancore, centralities)
vdf = centrality_graph(vgraph, centralities)
bdf = centrality_graph(bgraph, centralities)
fdf = centrality_graph(fgraph, centralities)
pdf = centrality_graph(pgraph, centralities)

print("Human: " + str(len(df['cents'])))
print("Virus: " + str(len(vdf['cents'])))
print("Bacteria: " + str(len(bdf['cents'])))
print("Fungi: " + str(len(fdf['cents'])))
print("Protozoa: " + str(len(pdf['cents'])))

plt.clf()
plt.yscale("log")
plt.xscale("log")
plt.ylabel("Fraction of Proteins")
plt.xlabel("Centrality of Human Protein in Human PPI Network")
plt.plot(df['cents'], df['percents'], label = 'human')
plt.plot(vdf['cents'], vdf['percents'], label = 'virus')
plt.plot(bdf['cents'], bdf['percents'], label = 'bacteria')
plt.plot(fdf['cents'], fdf['percents'], label = 'fungi')
plt.plot(pdf['cents'], pdf['percents'], label = 'protozoa')
plt.legend()
plt.show()
'''
phi_data_dict = {}
with open('phi_data-3.csv', encoding='latin1') as phi_data_file:
    reader_obj = csv.reader(phi_data_file)
    i = 0
    for row in reader_obj:
        if row[0] in phi_data_dict.keys():
            phi_data_dict[row[0]].add(row[5])
        else:
            phi_data_dict[row[0]] = {row[5]}

# go terms setup
import goatools
from goatools import obo_parser
from goatools.obo_parser import OBOReader
from goatools.obo_parser import GOTerm
from goatools.obo_parser import GODag

#This section of code was taken from StackOverflow to solve the problem of version 2.2 files not being able to be parsed by 
#Bio.UniProt.GOA

GAF22FIELDS = [
    "DB",
    "DB_Object_ID",
    "DB_Object_Symbol",
    "Qualifier",
    "GO_ID",
    "DB:Reference",
    "Evidence_Code",
    "With",
    "Aspect",
    "DB_Object_Name",
    "DB_Object_Synonym",
    "DB_Object_Type",
    "Taxon",
    "Date",
    "Assigned_By",
    "Annotation_Extension",
    "Gene_Product_Form_ID",
]

def gaf22iterator(handle):
    for inline in handle:
        if inline[0] == "!":
            continue
        inrec = inline.rstrip("\n").split("\t")
        if len(inrec) == 1:
            continue
        inrec[3] = inrec[3].split("|")  # Qualifier
        inrec[5] = inrec[5].split("|")  # DB:reference(s)
        inrec[7] = inrec[7].split("|")  # With || From
        inrec[10] = inrec[10].split("|")  # Synonym
        inrec[12] = inrec[12].split("|")  # Taxon
        yield dict(zip(GAF22FIELDS, inrec))

humanfile = r"C:\Users\Amrita\Documents\CS3824\geneontology\goa_human.gaf"
human_annotations = {}
for entry in gaf22iterator(open(humanfile)):
    uniprot_id = entry.pop('DB_Object_ID')
    human_annotations[uniprot_id] = entry

go_dag = GODag(optional_attrs='relationship_set')

# THERE IS EITHER A PROBLEM HERE 
human_annotations_transferred = dict()
for gene in human_annotations.keys():
    if gene in go_dag.keys():
        parents = go_dag[gene].get_all_parents()
        anscestorgraph = nx.DiGraph()
        for parent in parents:
            if parent in human_annotations.keys():
                anscestorgraph.add_edge(gene, parent)
    human_annotations_transferred[gene] = anscestorgraph   

# OR HERE, IF YOU COMMENT ONE OF THESE SECTIONS OUT 
# IT SHOULD WORK
human_annotations_transferred = dict()
for gene in human_annotations.keys():
    try:
        gograph = go_dag[gene].nodes()
        graph = nx.DiGraph()
        for node in gograph:
            if node in human_annotations.keys():
                graph.add_edge(gene, node)
        human_annotations_transferred[gene] = graph
    except:
        print('fail')

print(human_annotations_transferred[list(human_annotations_transferred.keys())[5]].nodes())
# IF EVERYTHING ABOVE WORKED, "['GO:0042843', 'GO:0042732', 'GO:0008152', 'GO:0019321', 'GO:0005996', 'GO:0016052', 'GO:0008150', 'GO:0005975', 'GO:0044238', 'GO:0044281', 'GO:0009056']"
# SHOULD PRINT 

# THIS IS TESTING THIS ON PROTOZOA DATA
humannodes = len(list(G.nodes()))
for node in pgraph.nodes():
    if node in G.nodes():
        print(len(list(G.neighbors(node)))/len(G.nodes())/(len(human_annotations[node]['With']) / humannodes) > 1)
        
# IF ABOVE RAN PROPERLY THE BELOW SHOULD PRINT
'''
False
True
True
False
True
True
False
True
True
True
True
True
True
False
True
False
True
True'''
# IF YOU WANT RAW NUMBERS REMOVE THE INEQUALITY IN THE PRINT